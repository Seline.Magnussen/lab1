package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while (true) {
            System.out.println("Let's play round " + roundCounter);
            System.out.println("Your choice (Rock/Paper/Scissors)?");
            String myMove = sc.nextLine();
            int compRan = (int) (3 * Math.random()) + 1;
            String compChoice = "";

            if (!rpsChoices.contains(myMove)) {
                System.out.println("I don't understand" + " " + myMove + " " + "Could you please try again? ");
                System.out.println("Your choice (Rock/Paper/Scissors)? ");
                myMove = sc.nextLine();
            }
            if (compRan == 0) {
                compChoice = "rock";
            } else if (compRan == 1) {
                compChoice = "paper";
            } else {
                compChoice = "scissors";
            }


            if (myMove.equals(compChoice)) {
                System.out.println("Human chose " + myMove + "," + " " + "computer chose " + compChoice + "." + " It's a tie!");
                System.out.println("Score: human " + humanScore +"," + " " + "computer " + computerScore);
            } else if ((myMove.equals("rock")) && compChoice.equals("scissors") || (myMove.equals("scissors")) && compChoice.equals("paper") || (myMove.equals("paper") && compChoice.equals("rock"))) {
                System.out.println("Human chose " + myMove + "," + " " + "computer chose " + compChoice + "." + " Human wins!");
                humanScore++;
                System.out.println("Score: human " + humanScore +"," + " " + "computer " + computerScore);
            } else {
                System.out.println("Human chose " + myMove + "," + " " + "computer chose " + compChoice + "." + " Computer wins!");
                computerScore++;
                System.out.println("Score: human " + humanScore +"," + " " + "computer " + computerScore);
            }
            roundCounter++;
            System.out.println("Do you wish to continue playing? (y/n)?");
            String cont = sc.nextLine();
            if (cont.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }

        }
        }



    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }}


